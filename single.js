class Single {
    constructor() {
        this.url_params = new URLSearchParams(window.location.search)
        const lang = this.url_params.get("lang")
        this.locale = lang == "en-US" ? (new LocaleEnglish()) : (new LocaleDutch())
        this.today = new Date()
    }

    generate() {
        const source = this.url_params.get("source") || "text"
        const text = this["generate_" + source]()
        const element = document.querySelector("text")
        element.textContent = text
        if (parseInt(text))
            element.classList.add("number")
    }

    generate_wkday() {
        return this.locale.wkday(this.today.getDay())
    }

    generate_day() {
        return this.today.getDate()
    }

    generate_mnth() {
        return this.locale.mnth(this.today.getMonth())
    }

    generate_text() {
        return this.url_params.get("text")
    }
}
   
window.addEventListener("load", () => (new Single()).generate())
