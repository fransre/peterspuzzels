window.addEventListener("load", () => {
    const puzzles = ["rotterdam", "rhombus", "digital"]
    const currency = document.querySelector(".order").dataset.currency

    function format_count(count) {
        return count ? count : ""
    }

    function parse_count(name) {
        const value = document.querySelector(`#count_${name}`).value
        return Math.abs(parseInt(value) || 0)
    }

    function format_price(price, nothing = "") {
        return price ? `${price < 0 ? "−" : ""}${currency}${Math.abs(price)}` : nothing
    }

    function parse_price(selector) {
        const value = document.querySelector(selector).value
        return parseInt(value.replace("−", "-").replace(currency, ""))
    }

    function compute_total() {
        const puzzle_counts = puzzles.map(name => [name, parse_count(name)])
        const counts = new Map(puzzle_counts)
        const names = Array.from(counts.keys())
        const subtotals = new Map(names.map(name => [name, counts.get(name) * parse_price(`#price_${name}`)]))
        names.forEach(name => {
            document.querySelector(`#count_${name}`).value = format_count(counts.get(name))
            document.querySelector(`#subtotal_${name}`).value = format_price(subtotals.get(name))
            document.querySelector(`#row_${name}`).classList.toggle("dimmed", counts.get(name) == 0)
        })
        const shippingcost = parse_price("input[name='\$shippingcost']:checked")
        document.querySelector("#subtotal_shippingcost").value = format_price(shippingcost, currency + "0")
        const grand_total = Array.from(subtotals.values()).reduce((sum, subtotal) => sum + subtotal, shippingcost)
        document.querySelector("#grand_total").value = format_price(grand_total, "—")
    }

    function add_patterns_to_textareas() {
        const textareas = document.querySelectorAll("textarea")
        textareas.forEach(textarea => {
            textarea.addEventListener("input", validateTextarea)
            textarea.addEventListener("change", validateTextarea)
            textarea.addEventListener("propertychange", validateTextarea)
        })
        function validateTextarea(event) {
            this.setCustomValidity("")
            const ascii_pattern = /^[\x20-\x7F]*$/gs
            const has_only_ascii = ascii_pattern.test(this.value)
            if (has_only_ascii) {
                const custom_pattern = this.dataset.custom_pattern
                if (custom_pattern) {
                    const custom_regex = new RegExp(`^${custom_pattern}$`, "gs")
                    const follows_custom_pattern = custom_regex.test(this.value)
                    if (!follows_custom_pattern)
                        this.setCustomValidity(this.dataset.custom_error)
                }
            }
            else
                this.setCustomValidity(this.dataset.ascii_error)
            this.reportValidity()
        }
    }

    function initialize() {
        const query_string = window.location.search
        const url_params = new URLSearchParams(query_string)
        puzzles.forEach(name => {
            const count = url_params.get(`count_${name}`)
            const element = document.querySelector(`#count_${name}`)
            element.value = count
            element.addEventListener("input", compute_total)
        })
        const radiobuttons = document.querySelectorAll("input[name='\$shippingcost']")
        radiobuttons.forEach(radiobutton => radiobutton.addEventListener("click", compute_total))
        document.querySelector("#email").focus()
        compute_total()
    }

    initialize()
    add_patterns_to_textareas()
})
