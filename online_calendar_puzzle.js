class CalendarPuzzle {
    constructor() {
        this.puzzlemat = document.querySelector(".puzzlemat")
        const rectangle = this.puzzlemat.getBoundingClientRect()
        this.bounds = this.to_svg(this.new_point(rectangle.width, rectangle.height))
    }

    listen() {
        this.translate()
        this.puzzlemat.addEventListener("pointerdown", this.pointer_down.bind(this))
        this.puzzlemat.addEventListener("pointermove", this.pointer_move.bind(this))
        this.puzzlemat.addEventListener("pointerup", this.pointer_up.bind(this))
        this.puzzlemat.addEventListener("pointercancel", event => event.preventDefault())
        this.puzzlemat.addEventListener("touchstart", event => event.preventDefault())
        this.puzzlemat.addEventListener("touchend", event => event.preventDefault())
        this.puzzlemat.addEventListener("touchcancel", event => event.preventDefault())
        this.puzzlemat.addEventListener("contextmenu", event => event.preventDefault())
    }

    translate() {
        const url_params = new URLSearchParams(window.location.search)
        const lang = url_params.get("lang") || "nl-NL"
        this.puzzlemat.setAttribute("lang", lang)
        const locale = lang == "en-US" ? (new LocaleEnglish()) : (new LocaleDutch())
        this.puzzlemat.querySelectorAll(".translate").forEach(element => {
            const match = element.getAttribute("id").match(/^(?<function_name>\w+)(?<has_index>\[(?<index>\d+)\]|)$/)
            const index = match.groups.has_index ? match.groups.index : undefined
            element.textContent = locale[match.groups.function_name](index)
        })
    }

    startingpoint(piece, point) {
        if (point) {
            piece.dataset.startingpoint_x = point.x
            piece.dataset.startingpoint_y = point.y
        }
        else {
            const x = 1 * piece.dataset.startingpoint_x
            const y = 1 * piece.dataset.startingpoint_y
            return this.new_point(x, y)
        }
    }

    handle_first_click(piece, click_point) {
        piece.dataset.moving = "true"
        piece.dataset.moved = "false"
        const previous_piece = this.selected_piece()
        if (previous_piece)
            previous_piece.dataset.reselected = "false"
        piece.dataset.reselected = piece.classList.contains("selected") ? "true" : "false"
        this.select(piece)
        this.put_on_top(piece)
        const start = this.new_point(this.position(piece).matrix.e, this.position(piece).matrix.f)
        this.startingpoint(piece, this.difference(click_point, start))
    }

    handle_click_sequence(piece) {
        const number_of_clicks = piece.dataset.clicks
        if (number_of_clicks > 1)
            this.unselect(piece)
        if (number_of_clicks == 2)
            this.rotate(piece)
        if (number_of_clicks == 3)
            this.flip(piece)
        piece.dataset.clicks = 0
    }

    pointer_down(event) {
        const piece = event.target.classList.contains("piece") ? event.target : undefined
        const previous_piece = this.selected_piece()
        if (piece) {
            if ("timer" in piece.dataset)
                clearTimeout(piece.dataset.timer)
            const clicks_before = "clicks" in piece.dataset ? parseInt(piece.dataset.clicks) : 0
            piece.dataset.clicks = clicks_before + 1
            if (piece.dataset.clicks == 1)
                this.handle_first_click(piece, this.eventpoint(event))
            piece.dataset.timer = setTimeout(() => this.handle_click_sequence(piece), 350)
        }
        else if (previous_piece) {
            const distance = this.difference(this.eventpoint(event), this.startingpoint(previous_piece))
            this.position_element(previous_piece, distance)
            this.snap_to_grid(previous_piece)
            this.unselect(previous_piece)
        }
    }

    pointer_move(event) {
        const piece = this.selected_piece()
        if (piece && piece.dataset.moving == "true") {
            const distance = this.difference(this.eventpoint(event), this.startingpoint(piece))
            const moved = this.position_element(piece, distance)
            piece.dataset.moved = moved ? "true" : "false"
        }
    }

    pointer_up(event) {
        const piece = this.selected_piece()
        if (!piece)
            return
        if (this.out_of_bounds(event))
            this.reset(piece)
        if (piece.dataset.moving == "true") {
            this.snap_to_grid(piece)
            piece.dataset.moving = "false"
            if (piece.dataset.moved == "true")
                this.unselect(piece)
        }
        if (piece.dataset.reselected == "true") {
            this.unselect(piece)
            piece.dataset.reselected = "false"
        }
    }

    out_of_bounds(event) {
        const point = this.eventpoint(event)
        return point.x < 0 || point.x > this.bounds.x || point.y < 0 || point.y > this.bounds.y
    }

    reset(piece) {
        this.position(piece).setTranslate(0, 0)
    }

    empty_transformation() {
        return this.puzzlemat.createSVGTransform()
    }

    new_transformation(func) {
        const matrix = this.empty_transformation()
        func(matrix)
        return matrix
    }

    rotation_angle() {
        return 90
    }

    rotate(piece) {
        const transformation = this.new_transformation(matrix => matrix.setRotate(this.rotation_angle(), 0, 0))
        this.transform(piece, transformation)
    }

    flip(piece) {
        const transformation = this.new_transformation(matrix => matrix.setScale(-1, 1))
        this.transform(piece, transformation)
        if ("flipped" in piece.dataset) {
            const href = piece.getAttribute("href")
            const flipped = piece.dataset.flipped
            piece.setAttribute("href", flipped)
            piece.dataset.flipped = href
        }
    }

    transform(piece, transformation) {
        const middlecenter_before = this.middlecenter(piece)
        piece.transform.baseVal.insertItemBefore(transformation, 1)
        const middlecenter_after = this.middlecenter(piece)
        const correction = this.difference(middlecenter_before, middlecenter_after)
        const movement = this.new_transformation(matrix => matrix.setTranslate(correction.x, correction.y))
        piece.transform.baseVal.insertItemBefore(movement, 1)
        this.snap_to_grid(piece)
    }

    position(element) {
        if (element.transform.baseVal.numberOfItems == 0) {
            const maxtrix = this.empty_transformation()
            element.transform.baseVal.insertItemBefore(maxtrix, 0)
        }
        return element.transform.baseVal.getItem(0)
    }

    grid() {
        /* abstract */
    }

    snap_radius() {
        /* abstract */
    }

    snap_to_grid(piece) {
        const middlecenter_old = this.middlecenter(piece)
        const grid = this.grid().map(coordinates => this.new_point(coordinates[0], coordinates[1], coordinates[2]))
        const point_with_shortest_distance = grid.reduce((closest_point_with_distance, point) => {
            const distance = this.distance(point, middlecenter_old)
            return distance < closest_point_with_distance[1] ? [point, distance] : closest_point_with_distance
        }, [undefined, Infinity])
        const shortest_distance = point_with_shortest_distance[1]
        if (shortest_distance < this.snap_radius()) {
            const middlecenter_new = point_with_shortest_distance[0]
            if (middlecenter_new.z < 0)
                this.put_on_bottom(piece)
            const correction = this.difference(middlecenter_new, middlecenter_old)
            const movement = this.new_transformation(matrix => matrix.setTranslate(correction.x, correction.y))
            piece.transform.baseVal.insertItemBefore(movement, 1)
        }
    }

    position_element(element, point) {
        if (this.distance(this.new_point(0, 0), point) < 0.01)
            return false
        this.position(element).setTranslate(point.x, point.y)
        return true
    }

    select(piece) {
        this.puzzlemat.querySelectorAll(".selected").forEach(previous_piece => this.unselect(previous_piece))
        piece.classList.add("selected")
    }

    unselect(piece) {
        piece.classList.remove("selected")
    }

    selected_piece() {
        return this.puzzlemat.querySelector(".selected")
    }

    put_on_top(piece) {
        piece.parentElement.appendChild(piece)
    }

    put_on_bottom(piece) {
        piece.parentElement.insertBefore(piece, piece.parentElement.firstChild)
    }

    new_point(x, y, z) {
        return new DOMPointReadOnly(x, y, z)
    }

    difference(point1, point2) {
        return this.new_point(point1.x - point2.x, point1.y - point2.y)
    }

    distance(point1, point2) {
        return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2))
    }

    to_svg(point) {
        return point.matrixTransform(this.puzzlemat.getScreenCTM().inverse())
    }

    eventpoint(event) {
        return this.to_svg(this.new_point(event.clientX, event.clientY))
    }

    middlecenter(element) {
        const rectangle = element.getBoundingClientRect()
        return this.to_svg(this.new_point(rectangle.x + rectangle.width / 2, rectangle.y + rectangle.height / 2))
    }
}
