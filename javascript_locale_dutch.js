class LocaleDutch {
    weekday(index) {
        return ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"][index]
    }

    wkday(index) {
        return ["zo", "ma", "di", "wo", "do", "vr", "za"][index]
    }

    month(index) {
        return ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"][index]
    }

    mnth(index) {
        return ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"][index]
    }

    url() {
        return "peterspuzzels.nl"
    }

    difficulty(index) {
        return ["erg makkelijk", "makkelijk", "iets makkelijker", "gemiddeld", "gemiddeld", "iets moeilijker", "moeilijk", "erg moeilijk"][index]
    }

    comparative(index) {
        return ["makkelijker", "makkelijker", "makkelijker", "makkelijker", "moeilijker", "moeilijker", "moeilijker", "moeilijker"][index]
    }

    conjunction() {
        return "maar"
    }

    fulldate(date) {
        return this.weekday(date.getDay()) + " " + date.getDate() + " " + this.month(date.getMonth()) + " " + date.getFullYear()
    }
}
