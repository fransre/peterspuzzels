class BaseDigital extends Base {
    constructor() {
        super()
        new FontFace("DS-Digital-Bold", "url(font_ds_digib.ttf)").load().then(font => document.fonts.add(font))
    }

    generate_day02d() {
        return String(this.today.getDate()).padStart(2, "0")
    }

    generate_month02d() {
        return String(this.today.getMonth() + 1).padStart(2, "0")
    }

    generate_year04d() {
        return String(this.today.getFullYear()).padStart(4, "0")
    }
}

window.addEventListener("load", () => new BaseDigital().generate())
