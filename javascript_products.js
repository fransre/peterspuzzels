window.addEventListener("load", () => {
    document.querySelectorAll(".choices > *").forEach(description => {
        description.addEventListener("click", event => {
            event.preventDefault()
            window.location.href = event.target.closest(".choices > *").querySelector("a").href
        })
    })
})
