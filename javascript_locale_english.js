class LocaleEnglish {
    weekday(index) {
        return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][index]
    }

    wkday(index) {
        return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][index]
    }

    month(index) {
        return ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][index]
    }

    mnth(index) {
        return ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][index]
    }

    url() {
        return "praxispuzzles.com"
    }

    difficulty(index) {
        return ["very easy", "easy", "a little easier", "average", "average", "a little harder", "hard", "very hard"][index]
    }

    comparative(index) {
        return ["easier", "easier", "easier", "easier", "harder", "harder", "harder", "harder"][index]
    }

    conjunction() {
        return "only"
    }

    fulldate(date) {
        return this.weekday(date.getDay()) + " " + this.month(date.getMonth()) + " " + date.getDate() + ", " + date.getFullYear()
    }
}
