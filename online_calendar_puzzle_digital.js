class CalendarPuzzleDigital extends CalendarPuzzle {
    grid() {
        return [
            [ 3, 3, -1],
            [ 3, 8,  0],
            [11, 3,  0],
            [11, 8, -1],
            [24, 3, -1],
            [24, 8,  0],
            [32, 3,  0],
            [32, 8, -1],
            [45, 3, -1],
            [45, 8,  0],
            [53, 3,  0],
            [53, 8, -1],
            [61, 3, -1],
            [61, 8,  0],
            [69, 3,  0],
            [69, 8, -1],
        ].map(coordinates => [coordinates[0] + 3.5, coordinates[1] + 3.5, coordinates[2]])
    }

    snap_radius() {
        return 4
    }
}

new CalendarPuzzleDigital().listen()
