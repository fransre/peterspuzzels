class Base {
    constructor() {
        this.today = new Date()
    }

    redirect_if_needed(hostname, pathname) {
        if (hostname.match(/^www\./))
            window.location.hostname = hostname.replace(/^www\./, "")
        if (pathname == "/" || pathname == "/index") {
            if (hostname == "praxispuzzles.com")
                window.location.pathname = "/calendar_puzzles"
            else if (hostname == "rotterdampuzzles.com")
                window.location.pathname = "/rotterdam_calendar_puzzle"
            else
                window.location.pathname = "/kalenderpuzzels"
        }
        if (hostname != "localhost" && pathname.match(/\.html\b/))
            window.location.pathname = pathname.replace(/\.html\b/, "")
    }

    rewrite_links() {
        document.querySelectorAll("a").forEach(link =>
            link.href = link.href
                .replace(/^https:\/\/peterspuzzels\.nl\//, "")
                .replace(/^https:\/\/praxispuzzles\.com\//, "")
                .replace(/^https:\/\/rotterdampuzzles\.com\//, "")
                .replace(/\?/, ".html?")
        )
    }

    replace_placeholders_with_specific_content() {
        document.querySelectorAll(".generate").forEach(element =>
            element.textContent = this["generate_" + element.getAttribute("id")](element.lang)
        )
        document.querySelectorAll(".general").forEach(element =>
            element.style.display = "none"
        )
        document.querySelectorAll(".specific").forEach(element =>
            element.style.display = "revert"
        )
    }

    generate() {
        this.redirect_if_needed(window.location.hostname, window.location.pathname)
        if (window.location.hostname == "localhost")
            this.rewrite_links()
        this.replace_placeholders_with_specific_content()
    }
}
