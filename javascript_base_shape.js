class BaseShape extends Base {
    constructor() {
        super()
        this.month_lengths = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        this.percentiles = [90, 75, 60, 50, 40, 25, 10, 0]
    }

    get_locale(lang) {
        return (lang || document.documentElement.lang) == "en-US" ? (new LocaleEnglish()) : (new LocaleDutch())
    }

    generate_wkday(lang) {
        return this.get_locale(lang).wkday(this.today.getDay())
    }

    generate_day() {
        return this.today.getDate()
    }

    generate_mnth(lang) {
        return this.get_locale(lang).mnth(this.today.getMonth())
    }

    generate_nextdate(lang) {
        var nextdate = this.today
        do {
            nextdate = new Date(nextdate.getFullYear() + 1, this.today.getMonth(), this.today.getDate())
        } while (nextdate.getDay() != this.today.getDay() || nextdate.getDate() != this.today.getDate())
        /* we must compare with getDate() because February 29, 2025 == March 1, 2025 */
        return this.get_locale(lang).fulldate(nextdate)
    }

    generate_count() {
        const number_of_previous_days_in_year = this.month_lengths.slice(0, this.today.getMonth()).reduce((sum, length) => sum + length, this.today.getDate() - 1)
        const index = number_of_previous_days_in_year * 7 + this.today.getDay()
        return this.number_of_different_solutions_per_day[index]
    }

    percentile() {
        const number_of_different_solutions = this.generate_count()
        const number_of_days_with_less_solutions = this.number_of_different_solutions_per_day.reduce((subtotal, number) => subtotal + (number < number_of_different_solutions ? 1 : (number === number_of_different_solutions ? 0.5 : 0)), 0)
        const total_number_of_days = this.number_of_different_solutions_per_day.length
        return Math.round(number_of_days_with_less_solutions / total_number_of_days * 100)
    }

    generate_difficulty(lang) {
        const percentile = this.percentile()
        const index = this.percentiles.findIndex(perc => percentile >= perc)
        return this.get_locale(lang).difficulty(index)
    }

    generate_comparative(lang) {
        const percentile = this.percentile()
        const index = this.percentiles.findIndex(perc => percentile >= perc)
        return this.get_locale(lang).comparative(index)
    }

    generate_percentile() {
        const percentile = this.percentile()
        return percentile <= 50 ? percentile : 100 - percentile
    }

    generate_conjunction(lang) {
        return this.generate_percentile() <= 25 ? this.get_locale(lang).conjunction() : ""
    }
}
